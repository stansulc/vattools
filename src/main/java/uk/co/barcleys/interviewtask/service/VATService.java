package uk.co.barcleys.interviewtask.service;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import uk.co.barcleys.interviewtask.domain.Rate;
import uk.co.barcleys.interviewtask.domain.VAT;

import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;

public class VATService {

    public static String JSONVAT_URL = "http://jsonvat.com/";

    public void printElements(int limit, List<Rate> rateList, Comparator<Rate> comparator) {

        rateList.stream().filter(r -> r.getPeriods() != null).sorted(comparator).limit(limit).forEach(r -> System.out.println(r.getName() + " " + r.getPeriods().get(0).getRates().getStandard()));

    }

    public VAT retrieveVAT() {

        ResteasyClient resteasyClient = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = resteasyClient.target(JSONVAT_URL);
        Response response = target.request().get();

        VAT vat = response.readEntity(VAT.class);

        return vat;

    }

    public void validateData() {
        // not implemented
    }

}
