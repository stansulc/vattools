package uk.co.barcleys.interviewtask.client;

import uk.co.barcleys.interviewtask.domain.Rate;
import uk.co.barcleys.interviewtask.domain.VAT;
import uk.co.barcleys.interviewtask.service.VATService;

public class VATClient {


    public static void main(String args[]) {

        System.out.println("I had to make some assumptions, because not all in the task is clearly defined and the scope is also not defined.");
        System.out.println("- The top/bottom 3 queries can be interpreted in several ways - I simply used the default streams limit function to make the cut.");
        System.out.println("- I omitted stuff like javadoc, logging, junit tests.");
        System.out.println("- There is no validation implemented, just an empty method. Depending on the source of data this is something that should be done in most cases. In this simple");
        System.out.println("    scenario I assume there is no \"null surprise\" and the first period is the current one (get(0) in the Period list).");
        System.out.println("- Also ommitted performance stuff, eg. service locator for the RestEasyClient\n");

        VATService vatService = new VATService();

        // get data
        System.out.println("About to fetch data\n");
        VAT vat = vatService.retrieveVAT();

        // validate data before sending it on
        vatService.validateData();

        // do the job
        System.out.println("About to print top 3 countries by standard VAT");
        vatService.printElements(3, vat.getRates(), (Rate r1, Rate r2) -> (Double.compare(r2.getPeriods().get(0).getRates().getStandard(), r1.getPeriods().get(0).getRates().getStandard())));
        System.out.println("");
        System.out.println("About to print bottom 3 countries by standard VAT");
        vatService.printElements(3, vat.getRates(), (Rate r1, Rate r2) -> (Double.compare(r1.getPeriods().get(0).getRates().getStandard(), r2.getPeriods().get(0).getRates().getStandard())));

    }

}
